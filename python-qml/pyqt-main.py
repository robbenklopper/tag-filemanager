import sys, os, xattr, itertools, collections

from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQml import qmlRegisterType, QQmlComponent, QQmlApplicationEngine, QQmlListProperty
from PyQt5.QtCore import QStringListModel, QUrl, QObject, pyqtProperty, pyqtSignal, pyqtSlot, QDir
from PyQt5.QtWidgets import QFileSystemModel, QApplication

class Tag(QObject):
    def __init__(self, display, parent=None):
        super().__init__(parent)
        self._display = display

    @pyqtProperty('QString')
    def display(self):
        return self._display

    @pyqtProperty('QString')
    def content(self):
        return self._display



class Directory(QObject):
    def __init__(self, dirPath='/', parent=None):
        super().__init__(parent)
        print("Directory constructor")
        self._currentDir = dirPath
        self._entries = [DirectoryEntry(os.path.join(self._currentDir,item)) for item in os.listdir(self._currentDir)]
        self._tagsInDir = collections.Counter(itertools.chain.from_iterable([entry._tags for entry in self._entries]))

        print("_tagsInDir: ", self._tagsInDir)
        #print("_entries: ", self._entries)

    entriesChanged = pyqtSignal()
    currentDirChanged = pyqtSignal()
    tagsInDirChanged = pyqtSignal()

    @pyqtSlot()
    def dirUp(self):
        print("going dir up…")
        self._currentDir = os.path.normpath(os.path.join(self._currentDir,'..'))
        self._entries = [DirectoryEntry(os.path.join(self._currentDir,item)) for item in os.listdir(self._currentDir)]
        self._tagsInDir = collections.Counter(itertools.chain.from_iterable([entry._tags for entry in self._entries]))
        self.currentDirChanged.emit()
        self.entriesChanged.emit()
        self.tagsInDirChanged.emit()



    @pyqtProperty(QQmlListProperty, notify=entriesChanged)
    def entries(self):
        print("getting directory")
        return QQmlListProperty(DirectoryEntry, self, self._entries)

    @pyqtProperty('QString', notify=currentDirChanged)
    def currentDir(self):
        return self._currentDir

    @currentDir.setter
    def currentDir(self, newDir):

        if os.path.isdir(newDir):
            self._currentDir = os.path.normpath(os.path.join(self._currentDir,newDir))
            self._entries = [DirectoryEntry(os.path.join(self._currentDir,item)) for item in os.listdir(self._currentDir)]
            self._tagsInDir = collections.Counter(itertools.chain.from_iterable([entry._tags for entry in self._entries]))
            self.currentDirChanged.emit()
            self.entriesChanged.emit()
            self.tagsInDirChanged.emit()
        else:
            print("can't change to non-existing dir: ", newDir)


#    tagsInDirChanged = pyqtSignal()
#    @pyqtProperty('QStringListModel')
#    def tagsInDir(self):
#        return sorted(self._tagsInDir.elements())

    @pyqtProperty(QQmlListProperty, notify=tagsInDirChanged)
    def tagsInDir(self):
        return QQmlListProperty(Tag, self, list(map(lambda x: Tag(x), sorted(self._tagsInDir.elements()))))


class DirectoryEntry(QObject):
    def __init__(self, absPath, parent=None):
        super().__init__(parent)
        #print("absPath: ", absPath)
        self._absPath = absPath
        self._dirname, self._basename =  os.path.split(absPath)
        self._isDir = os.path.isdir(self._absPath)
        #print("absPath: ", self._absPath)
        try:
            self._tags = xattr.getxattr(self._absPath, "user.tags").decode('utf-8').split(',')
        except:
            self._tags = []
        #print("tags: ", self._tags)

    @pyqtProperty('QString')
    def basename(self):
        return self._basename

    @pyqtProperty('QString')
    def absPath(self):
        return self._absPath
        #return "blablabla"

    @pyqtProperty(bool)
    def isDir(self):
        return self._isDir

#    @pyqtProperty('QStringListModel')
#    def tags(self):
#        return self._tags

    @pyqtProperty(QQmlListProperty)
    def tags(self):
        return QQmlListProperty(Tag, self, list(map(lambda x: Tag(x), self._tags)))




class WorkingDir(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._pwd = os.getcwd()
        #self.model = QStringListModel()
        #self.model.setStringList(os.listdir(os.getcwd()))
        self.model = QStringListModel(os.listdir(os.getcwd()))
        #self._model = os.listdir(os.getcwd())

    pwdChanged = pyqtSignal()

    @pyqtSlot()
    def bla(self):
        print("bla bla")
        self.pwd = os.path.normpath(os.path.join(self._pwd, '..'))
        #self.pwd = "/bla/"
        #self.pwd(self, "/home/simon")

    @pyqtProperty('QString', notify=pwdChanged)
    def pwd(self):
        return self._pwd

    @pwd.setter
    def pwd(self, newPwd):
        print("attempt to change dir…")
        oldPwd = self._pwd
        try:
            os.chdir(newPwd)
            self._pwd = os.getcwd()
        except FileNotFoundError:
            self._pwd = oldPwd
        print("pwd is: ",self._pwd)
        self.model.setStringList(os.listdir(os.getcwd()))
        #self._model = os.listdir(os.getcwd())
        self.pwdChanged.emit()







# Create the application instance.



if __name__ == "__main__":


    workingDir = WorkingDir()
    directory = Directory(os.path.join(os.getcwd(), "test-resources"))

    #app = QGuiApplication(sys.argv)
    app = QApplication(sys.argv)



    # Create a QML engine.
    engine = QQmlApplicationEngine()

    #https://www.riverbankcomputing.com/static/Docs/PyQt5/api/qtwidgets/qfilesystemmodel.html?highlight=filesystemmodel#PyQt5.QtWidgets.QFileSystemModel
    #http://python.6.x6.nabble.com/Segmentation-fault-with-QFileSystemModel-td1794952.html
    #model = QFileSystemModel()
    #model.setRootPath('/home/simon/.bin')
    #model.setRootIndex('/drucken')

    qmlRegisterType(DirectoryEntry)
    qmlRegisterType(Directory)
    qmlRegisterType(Tag)
    #engine.rootContext().setContextProperty("dirModel2",model)
    engine.rootContext().setContextProperty("directory",directory)
    #engine.rootContext().setContextProperty("dirModel", workingDir.model)
    #engine.rootContext().setContextProperty("workingDir", workingDir)

    engine.load("main.qml")
    engine.quit.connect(app.quit)
    sys.exit(app.exec_())
