import sys, os
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtCore import QUrl, QStringListModel

print("bla")

if __name__ == '__main__':
    model = QStringListModel()
    #list = QStringList()
    #model.setStringList(os.listdir(os.getcwd()))
    model.setStringList(os.listdir('/home/simon'))

    #os.chdir('/bin')
    print(os.listdir(os.getcwd()))
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()
    engine.rootContext().setContextProperty("dirModel", model)

    engine.load(QUrl("main.qml"))

    if not engine.rootObjects():
        print("existing…")
        sys.exit(-1)

    sys.exit(app.exec_())

