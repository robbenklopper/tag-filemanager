import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQml.Models 2.12

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Scroll")

    id: mainWindow

    //https://stackoverflow.com/a/3426956
    function hashCode(str) {
        // java String#hashCode
        var hash = 0
        for (var i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash)
        }
        return hash
    }

    function intToRGB(i) {
        var c = (i & 0x00FFFFFF).toString(16).toUpperCase()

        return "00000".substring(0, 6 - c.length) + c
    }
    MouseArea {
        acceptedButtons: Qt.BackButton
        anchors.fill: parent

        onClicked: {
            if (mouse.button == Qt.BackButton) {
                console.log("back button hit")
                directory.dirUp()
            }
        }
    }

    Column {

        anchors.fill: parent
        RowLayout {

            id: toolbar
            width: parent.width

            //height: parent.height
            //anchors.bottom: scrollView.top
            //anchors.fill: parent.fill

            //            Button {
            //                id: firstbutton
            //                text: "blablab"
            //                //onClicked: dummyModel.append({name: "|" + Math.random() + "|"})
            //                //onClicked: dirModel.append({display: "bla"})
            //                onClicked: workingDir.bla()
            //            }
            Button {
                id: goup
                text: ".."
                //onClicked: firstbutton.text = "bla1"
                onClicked: directory.dirUp()
            }

            TextInput {
                text: directory.currentDir
                height: parent.height
                //width: parent.width - goup.width
                color: "green"
                leftPadding: 20
                rightPadding: 20

                //anchors.right: parent.right
                //anchors.fill: parent.anchors
                Keys.onReturnPressed: {
                    console.log("return pressed")
                    directory.currentDir = text
                }
            }

        }

        Row {
            anchors.top: toolbar.bottom
            height: parent.height - toolbar.height
            //anchors.bottom: parent.bottom
            //anchors.fill: parent
            ColumnLayout {
                id: tagcloud
                anchors.top: parent.top

                Text {
                    text: qsTr("Tags:")
                }

                Column {
                    spacing: 5
                    Repeater {
                        model: directory.tagsInDir
                        Rectangle {
                            height: childrenRect.height + 5
                            width: childrenRect.width + 5
                            color: "#" + intToRGB(hashCode(display))
                            Text {
                                anchors.centerIn: parent
                                text: display
                            }

                            Component.onCompleted:  {
                                console.log("display: " + display)
                                console.log("hash: " + hashCode(display))
                                console.log("intToRGB: " + intToRGB(hashCode(display)))
                            }
                        }
                    }
                }
            }

            ScrollView {
                id: scrollView
                clip: true
                anchors.top: parent.top
                //anchors.top: row.bottom
                anchors.bottom: parent.bottom
                //anchors.right: parent.right
                //anchors.fill: parent.bottom
                ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ScrollBar.vertical.policy: ScrollBar.AlwaysOn
                //width: mainWindow.width
                //TODO: use correct way to set width implicitly
                contentWidth: mainWindow.width - tagcloud.width

                Flickable {
                    boundsBehavior: Flickable.StopAtBounds
                    contentHeight: folderContentFlow.height
                    Flow {
                        id: folderContentFlow
                        //width: mainWindow.width
                        spacing: 5
                        width: parent.width

                        Repeater {
                            id: repeater
                            model: directory.entries

                            Rectangle {
                                id: folderItem
                                height: childrenRect.height + 5
                                width: childrenRect.width + 5
                                border.width: model.isDir ? 4 : 1

                                //console.log("model data: " + modelData)
                                //color: "red"
                                Button {
                                    id: folderButton
                                    //anchors.centerIn: parent
                                    //text: qsTr(name)
                                    text: model.basename
                                    //text: model.tags
                                    //text: modelData
                                    onClicked: {
                                        if (model.isDir) {
                                            //TODO: fix that shit, needs to be platform independent, use pyhton os.path api
                                            console.log("going to: " + directory.currentDir
                                                        + "/" + model.basename)
                                            directory.currentDir = directory.currentDir
                                                    + "/" + model.basename
                                        } else {
                                            console.log("error, item not a dir!")
                                        }
                                    }
                                }

//                                Component.onCompleted: {
//                                    console.log(model.basename)
//                                    console.log("dumping tags: ")
//                                    console.log(model.tags.length)
//                                }

                                Row {
                                    spacing: 3
                                    anchors.top: folderButton.bottom
                                    Repeater {
                                        model: tags
                                        Rectangle {
                                            height: childrenRect.height + 10
                                            //width: childrenRect.width + display.length
                                            width: childrenRect.width + 10
                                            color: "#" + intToRGB(hashCode(display))
//                                            Text {
//                                                anchors.centerIn: parent
//                                                font.pointSize: 9
//                                                text: content
//                                            }
                                        }
                                    }
                                }
                            }
                        }


                    }
                }
            }
        }
    }
}
